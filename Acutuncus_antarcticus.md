# Acutuncus antarcticus

## Quality control

Quality control on raw reads was performed with fastqc + multiqc. 

## Trimming

Trimming was performed using fastp, by setting parameters accordingly to the Quality Contron on raw reads.

## Read preparation and normalization

Before assembly trimmed reads of each sample were merged together in order to obtain only une R1.fastq and one R2.fastq. These two files were then normalized with bbnorm in order to reduce assembly time. The max read copy number was set to 100 and the minimum was set to 1 in order to be as conservative as possible. 

## Transcriptome assembly

The transcriptome was assembled using the Oyster River Protocol pipeline. 

The assembled transcriptome has 118895 contigs

The BUSCO against metazoa of the transcriptome:
```
C:86.6%[S:43.5%,D:43.1%],F:6.9%,M:6.5%,n:978
```

## Expression analysis

### Triplicate scatterplots

Scatterplots are produced in R using the log(tpm+1) of the readmapping of alla samples on transcriptome, in a pairwise fashion within each triplicate. A total of 18 scatterplots was produced.  

#### Issues

I suspect there is contamination from another species because scatter plots should all look like this since they are performed within triplicates.

![20201008105548](img/20201008105548.png)

But there are some scatter plots that look lke this:

![20201008105801](img/20201008105801.png)

showing a variability too high for a normal biological replica. What follows is an attempt to identify an remove this contamination.

The contig GC content distribution is a bit odd, but does not show two separate peaks.

![20201008170302](img/20201008170302.png)


#### Kraken analysis

Analysis of species composition or each sample was performed with krarken2 using the whole NT database and the read1 from each sample.


#### Enrichment analysis

#### COI analysis

Human COI protein sequence was downloaded from NCBI and used in a BLAST search against the transcriptome in order to identify all the assembled COI genes. The sequences with a valid hit were then extracted and classified by BLAST on NCBI database. 

A mapping of reads from each sample was performed using salmon against the COI sequences found and expression values are reported in the image below. 

![20201007160536](img/20201007160536.png)

The only expression of COI genes is not a great quantitative proxy for contamination, but it helps. Looks like there are some samples that have a contamination from *Patella* that is not negligible. 


#### filtering with genomes

The assembled transcriptome was filtered by blasting it against a nucleotide database derived from the genomes of *Hypsibius dujardini* and *Ramazzottius varieornatus*. 

```
makeblastdb -i combined_genomes.fasta -dbtype nucl
```

We do not keep the seqids of the genomes as we only care about blast hits.

##### Classic blastn

```
blastn -db ../genomi/Hypduj+Ramvar_genomes.fasta  -query Acutuncus_norm.ORP.fasta -evalue 10 -outfmt 6 -out Acutuncus_vs_Tardigrada_genomes_10.tsv -num_threads 100
```
Sequences with at least one hit are too few (4893) to build a reasonable transcriptome, the process needs to be tuned. We need to take into account introns and exons that fragment the alignment. 

The first solution is to lower the wordsize, from 11 to 9

##### wordsize reduction

```
blastn -db ../genomi/Hypduj+Ramvar_genomes.fasta  -query Acutuncus_norm.ORP.fasta -evalue 10 -outfmt 6 -out Acutuncus_vs_Tardigrada_genomes_10.tsv -num_threads 100 -word_size 9
```
it is musch slower now

There are 118789 hits with evalue <= 10. Only ~100 seqs were removed.

There are 45734 hits with evalue <= 1e-5. This number is much more reasonable. Extracting the sequences and evaluating completeness with BUSCO will follow.

###### BUSCO analysis

```
C:81.9%[S:42.8%,D:39.1%],F:5.2%,M:12.9%,n:978
```
Missing BUSCOs roughly doubled.
Trying again with evalue 1e-3

###### GC content

looks like GC content got a little bit better

![20201008170037](img/20201008170037.png)

this is for filtering with 1e-3 threshold, with 1e-5 is almost identical

###### Resulting scatterplots

By scatterplot analysis the contamination appears to be reduced, but not much

The problem may be realted to several conserved genes between *Acutuncus* and *Patella*.

#### Reassembly from fewer samples

By analizing each sample with all the approaches described befere i can try to identify those samples that appear to have very little to no contamination. This is not a very scientific approach, it's more like an educated guess with data support. 

By using the results from the previuos analysis we can build this table (it also contains metadata):

![20201008110508](img/20201008110508.png)
+ **sample** is the sample name
+ **has contamination R** has "yes" value when the scatterplot looks very noisy
+ **perc of protostomia kraken** is the percentage of reads from protostomia belonging to spiralia in the kraken report. Here cells in green highlight values less than a standard deviation unit higher than the median, red cells a value higher than median + standard deviation
+ **has contamination COI** is "yes" when the counts for COI belonging to *Patella* outnumber those belonging to *tardigrada*
+ **coi perc** is the percentage of COI counts belonging to *Patella* over the total COI counts. The cell color is just a color scale mapping

Using this table as a reference I am deciding to reassemble the reference transcriptome from those samples that have all good values:

> S11  
S19  
S20  
S21  
S31  
S32  
S33  
S8

```bash
for sample in S11 S19 S20 S21 S31 S32 S33 S8
	do
	file=`find . -name "*_${sample}_*.1.Trimmed.fastq.gz"`
	echo $file
	cat $file >> ../reassembly/Acuant_selected_trimmed.1.fastq.gz 
	file=`find . -name "*_${sample}_*.2.Trimmed.fastq.gz"`
	echo $file
	cat $file >> ../reassembly/Acuant_selected_trimmed.2.fastq.gz
	done
```
A step of normalization with bbnorm.sh follows, with the same settings used previously.

The assembled transcriptome (orp) has 95511 contigs, percent GC is 45.82 and N50 is 489.

busco (metazoa) results are:
```
C:60.3%[S:44.8%,D:15.5%],F:28.3%,M:11.4%,n:978
```
We can compare this method with the BLAST-on-genome filtering by means of GC content distribution of contigs:

![20201009121459](img/20201009121459.png)

It is clear that the BLAST filtering creates a sharper peak, but selecting samples also created a better peak.


#### Identification of contaminations by removal of conserved genes

Follow this flowchart:
<div align="center">

![20201014160945](img/20201014160945.png)

</div>

```
diamond blastp -p 64 -d ~/Shared/Databases/uniprot/uniprot_sprot.dmnd -q GCA_001949185.1_Rvar_4.0_protein.faa -e 1e-5  -o Ramvar_uniprot_hits.tsv -f 6 qseqid sseqid evalue
```
8932 sequences with a hit, 14075 sequences with no hit.

```
diamond makedb --in Ramvar_proteins_not_hits_uniprot.fasta --db Ramvar_proteins_not_hits_uniprot.dmnd -p 64
diamond blastp -p 64 -d ~/Shared/Databases/uniprot/uniprot_sprot.dmnd -q GCA_001949185.1_Rvar_4.0_protein.faa -e 1e-5  -o Ramvar_uniprot_hits.tsv -f 6 qseqid sseqid evalue  
```

The clustermap for the expression of top 100 best hits (by evalue), calculated on the whole transcriptome is this:

```python
import seaborn as sns
import numpy as np

Acuant_expr = pd.read_csv("/mnt/DATA/home/samuele/Shared/Whole/Acutuncus_antarticus/expr1.1/Acutuncus_tpm", sep = "\t")

list_top100 = pd.read_csv("/mnt/DATA/home/samuele/Shared/Tardigrada/pulizia_Acutuncus/proteomi/best_100_Acuant.list", names = ["Name"])

sub_set = list_top100.merge(Acuant_expr, on = "Name", how = "inner")

sns.clustermap(np.log(sub_set.set_index("Name") + 1))
```

![20201014171915](img/20201014171915.png)

With the correlation matrix among the samples being this:

![20201015174735](img/20201015174735.png)



Here I can select the best looking samples for a new assembly since they look like expression of key hyper conserved genes is higher and more uniform. 

![20201015173925](img/20201015173925.png)

So the samples for reassembly are:
> S27  
S13  
S15  
S24  
S25  
S11  
S28  
S12  
S33  
S17  
S18  
S34  
S16  
S19

Two merged fastq were created as decribed above, assembly is carried out with orp.

#### Metatranscriptome approach

Here I will try to merge all the assembled transcriptomes to date which may have caused cross-contamination, then I will map the reads from *Acutuncys* samples and see what happens.

Implied species are for now:
+ *Acutuncus antarcticus*
+ *Cryptopygus terranovus*
+ *Patella*
+ *Pseudorchomene*

This approach should highlight again those samples with expression values that are too divergent. 

After loading the full expression matrix the correlation plot is like this:

![20201016122027](img/20201016122027.png)

which suggests the presence of at least 3 sources of contamination, this is supported by clustering the samples by the gene expression of genes that have a median expression > 100 in the metatranscriptome mapping.

![20201016123214](img/20201016123214.png)

Expression data is also used for MDS and plotted in 2D here, the color is based on the previuosly described selection. We can see here that the selected samples are pretty close together.

![20201016123917](img/20201016123917.png)


##### Meta mapping

This is done by mapping reads from all the 4 species used in the metatranscriptome on the metatranscriptome. Dimensionality reduction shows a good clustering (expression is measured in TPM):

![20201022115453](img/20201022115453.png)

![20201022115508](img/20201022115508.png)

These results suggest that the amount of contamination does not affect the expression data, and the transcriptomic divergence that we observe may not be due to heavy contamination of the samples, but to some other phenomena that are causing massive differences in the gene expression of *Acutuncus* (maybe Presence/absence variation?).

#### Filter by length approach

Sequences from the transcriptome that was blasted against the genome were filtered by length and only those longer than 300 were retained. The resulting transcriptome was analised in the same way as the previuos, but no difference in background noise could be observed.


#### Top 100 tardigrada sequences and reassembly

Sequences that were previously selected as the 100% conserved transcript exclusive to tardigrada (proteome from *Ramazzottius varieornatus* as reference) were mapped with all samples and mapping rate was evaluated. In theory mapping rate should be more or less uniform in all samples, and should also be semi-proportional to the amount of reads from tardigrada in each sample.  

Mapping rates are reported in the following table: 
<div align="center">

| sample 	| mapping rate|
|---------------|-------------|
|5C-5_S8_L001.expr | 0.18% |
|15C-LT-2_S20_L001.expr | 0.72% |
|20-LT-8_S28_L001.expr | 0.85% |
|20C-5C-BT-5_S31_L001.expr | 0.11% |
|20C-5C-BT-4_S30_L001.expr | 0.56% |
|10-LT-10_S16_L001.expr | 0.87% |
|20C-5C-LT-3_S33_L001.expr | 0.72% |
|10-LT-8_S14_L001.expr | 0.69% |
|15-BT-6_S17_L001.expr | 0.81% |
|20C-5C-BT-1_S29_L001.expr | 0.19% |
|15-BT-8_S19_L001.expr | 0.62% |
|20C-5C-LT-4_S34_L001.expr | 0.78% |
|5C-8_S10_L001.expr | 0.37% |
|10-BT-8_S12_L001.expr | 0.62% |
|15-BT-7_S18_L001.expr | 0.70% |
|20-BT-4_S23_L001.expr | 0.61% |
|10-BT-9_S13_L001.expr | 0.53% |
|20C-5C-LT-2_S32_L001.expr | 0.17% |
|5C-7_S9_L001.expr | 0.22% |
|15C-LT-5_S22_L001.expr | 0.19% |
|10-BT-7_S11_L001.expr | 0.91% |
|15C-LT-3_S21_L001.expr | 0.60% |
|10-LT-9_S15_L001.expr | 0.67% |
|20-LT-6_S26_L001.expr | 0.52% |
|20-BT-6_S24_L001.expr | 0.68% |
|20-BT-7_S25_L001.expr | 0.70% |
|20-LT-7_S27_L001.expr | 0.77% |

</div>

There are 5 samples with a clearly higher mapping rate (S15, S16, S17, S18, S19), interestingly they are consecutive numbers. Those are used to assemble another transcriptome.

Reads are then mapped to such transcriptome.

## Differential expression analysis

### Explorative analysis of contaminated transcriptome

While waiting for all the new assemblies i am proceeding with exploration of expression, hoping for a hint of results. 

```R
y = DGEList(Acuant.counts)

rownames(y$samples)

metadata = data.frame(sample = rownames(y$samples), species = "Acutuncus antarcticus")
metadata$Temp = c(rep("10C",6), rep("15C", 6), rep("20C",3), rep("5C", 6), rep("20C", 3), rep("5C", 3))
metadata$Group = "C"
metadata$Group[grep("LT", metadata$sample)] = "LT"
metadata$Group[grep("BT", metadata$sample)] = "BT"

y= calcNormFactors(y)

keep = filterByExpr(y)
y = y[keep,]

y=estimateDisp(y)

plotBCV(y)

mds = plotMDS(y)
plotdata = data.frame(Dim1 = mds$x, Dim2= mds$y, Temp = factor(metadata$Temp), Group = factor(metadata$Group))

oki = c("S27|S13|S15|S24|S25|S28|S12|S33|S17|S18|S34|S16|S19")

grep(oki, rownames(plotdata))

plotdata$contamination = "YES"
plotdata$contamination[grep(oki, rownames(plotdata))] = "NO"

library(ggplot2)
ggplot(plotdata, aes(Dim1, Dim2, color = contamination, shape = Group)) + geom_point(size=3)
```

The MDS of the edgeR filtered and normalized counts is 

![20201016130044](img/20201016130044.png)

and coloring the points by Temperature shows a bit of separation of the different experimental points. 

![20201016131249](img/20201016131249.png)

The differential expression analysis due to "contamination" showed 4444 DEGs which clustered like in the following heatmap

![20201016171420](img/20201016171420.png)

There are 3 evident main sources of contamination which appear to be different. By extracting the very highly expressed DEGs in the first column of the heatmap the enrichment test reavealed very few significant gene sets as enriched, confirming that the source of the differential expression is by contamination of another organism. **Analysis of sequences of such genes should reveal the source of the contamination. The same needs to be done with the other two columns at least**

## Idee
+ mappare tutte le bestie che ho sul trascrittoma di Acutuncus e poi clusterizzare in base alle distanze tra i valori di espressione di ciascuno
+ costruire un metatrascrittoma assemblando prima tutti i trascrittomi che posso e mapparci sopra le read che ho e fare il clustering. 

in entrambi i casi dovrei avere una certa idea almeno di quali sono i campioni contaminati (se ci sono)
